package com.mygdx.game.mapgen;

public class MapGen1 {

	public MapGen1() {
		// TODO Auto-generated constructor stub
	}

/*
	public SnafuSabotageMapGen(int size, int rooms, int num_players, int cpus) {
		super(size);

		int attempts = 0;
		restart: while (true) {
			Interval int_check = new Interval(10 * 1000, false); // Give us time to create the map
			attempts++;
			if (attempts > 100) {
				DSRWebServer.SendEmailToAdmin("Map Gen Timed out", "The map gen has timed out after " + attempts + " attempts.");
				throw new RuntimeException("Map generation timed out.");
			}

			super.clearMap();
			centres.clear();

			for (int r=1 ; r<=rooms ; r++) {
				byte x = Functions.rndByte(1, size-MAX_ROOM_SIZE-2);
				byte y = Functions.rndByte(1, size-MAX_ROOM_SIZE-2);
				byte w = Functions.rndByte(2, MAX_ROOM_SIZE);
				byte h = Functions.rndByte(2, MAX_ROOM_SIZE);

				if (r >= 1 && r<=num_players) { // Start room
					Point p = getRoomLoc(r, size);
					x = (byte)p.x; // Need to store these so we connect the rooms.
					y = (byte)p.y;
				} else if (r == num_players+1) { // Middle room
					x = (byte)((size/2) - (w/2));
					y = (byte)((size/2) - (h/2));
				} else {
					// Loop until we find an empty area
					while (super.isThereARoomAt(x, y, w, h) == true) {
						x = Functions.rndByte(1, size-MAX_ROOM_SIZE-2);
						y = Functions.rndByte(1, size-MAX_ROOM_SIZE-2);
						w = Functions.rndByte(2, MAX_ROOM_SIZE);
						h = Functions.rndByte(2, MAX_ROOM_SIZE);

						// Check if we've run out of time
						if (int_check.hitInterval()) {
							this.showMap();
							continue restart;
						}
					}
				}
				this.createRoomByTopLeft(x, y, w, h, TextureStateCache.TEX_INTERIOR1, (byte)(num_players*-1), true);
				// Store the rooms for checking later
				centres.add(new Point(x+(w/2), y+(h/2)));
			}

			// Connect rooms
			doors.clear();
			for (int i=0 ; i<centres.size() ; i++) {
				Point start = centres.get(i);
				Point end = centres.get(Functions.rnd(0, centres.size()-1));
				while (start == end) {
					end = centres.get(Functions.rnd(0, centres.size()-1));
				}
				//super.showMap();
				this.addCorridorAndDoors(start.x, start.y, end.x, end.y, false);
				//super.showMap();
			}

			// Check deployment rooms are connected
			boolean success = true;
			for (int s=0 ; s<num_players ; s++) {
				if (areRoomsConnected(centres, s, s+1) == false) {
					super.showMap();
					success = false;
					break;
				}
			}

			if (success) {
				addDoors();
				addComputers(cpus);
				break;
			}
			DSRWebServer.p("Recreating map.");

		}

	}


	private Point getRoomLoc(int side, int size) {
		switch (side) {
		case 1:
			return new Point(1, 1);
		case 2:
			return new Point(size-MAX_ROOM_SIZE-2, size-MAX_ROOM_SIZE-2);
		case 3:
			return new Point(1, size-MAX_ROOM_SIZE-2);
		case 4:
			return new Point(size-MAX_ROOM_SIZE-2, 1);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	private boolean areRoomsConnected(ArrayList<Point> centres, int s, int e) {
		AStar astar = new AStar(this);
		Point start = centres.get(s);
		Point end = centres.get(e);
		astar.findPath(start.x, start.y, end.x, end.y, false);
		if (astar.wasSuccessful()) {
			return true;
		} else {
			return false;
		}

	}


	private void addComputers(int cpus) {
		for (int i=0 ; i<cpus ; i++) {
			int x, y;
			while (true) {
				x = Functions.rnd(1, super.getMapWidth()-2);
				y = Functions.rnd(1, super.getMapHeight()-2);
				if (super.map[x][y].major_type == MapDataTable.MT_FLOOR) {
					// Check its next to a wall and not near a corridor
					ServerMapSquare sms[] = new ServerMapSquare[4];
					sms[0] = super.map[x-1][y];
					sms[1] = super.map[x+1][y];
					sms[2] = super.map[x][y-1];
					sms[3] = super.map[x][y+1];
					int wall_count = 0;
					for (int s=0 ; s<sms.length ; s++) {
						if (sms[s].major_type == MapDataTable.MT_FLOOR) {
							wall_count++;
						}
					}
					if (wall_count == 3) {
						break;
					}
				}
			}
			super.map[x][y].major_type = MapDataTable.MT_COMPUTER;
			super.map[x][y].deploy_sq_side = 0;
		}
	}
*/
}
