package com.mygdx.game.components;

import com.scs.astar.Waypoints;

public class AlienComponent {
	
	public float speed;
	public Waypoints route;
	
	public AlienComponent(float _speed) {
		speed = _speed;
	}
	
}
