package com.mygdx.game;

import com.badlogic.gdx.controllers.Controller;
import com.mygdx.game.components.AlienComponent;
import com.mygdx.game.components.CanCollectComponent;
import com.mygdx.game.components.CollisionComponent;
import com.mygdx.game.components.HarmOnContactComponent;
import com.mygdx.game.components.ImageComponent;
import com.mygdx.game.components.MovementComponent;
import com.mygdx.game.components.PlayersAvatarComponent;
import com.mygdx.game.components.PositionComponent;
import com.mygdx.game.models.PlayerData;
import com.scs.basicecs.AbstractEntity;

public class EntityFactory {

	private MyGdxGame game;

	public EntityFactory(MyGdxGame _game) {
		game = _game;
	}


	public AbstractEntity createPlayersAvatar(PlayerData player, Controller controller, int x, int y) {
		AbstractEntity e = new AbstractEntity("Player");

		ImageComponent imageData = new ImageComponent("greenblip.png", 1, Settings.PLAYER_SIZE, Settings.PLAYER_SIZE);
		e.addComponent(imageData);
		PositionComponent pos = PositionComponent.ByBottomLeft(x, y, Settings.PLAYER_SIZE, Settings.PLAYER_SIZE);
		e.addComponent(pos);
		CollisionComponent cc = new CollisionComponent(true, true);
		e.addComponent(cc);
		MovementComponent mc = new MovementComponent();
		e.addComponent(mc);
		PlayersAvatarComponent uic = new PlayersAvatarComponent(player, controller);
		e.addComponent(uic);
		CanCollectComponent ccc = new CanCollectComponent();
		e.addComponent(ccc);
		return e;
	}


	public AbstractEntity createWall(int x, int y, float w, float h) {
		AbstractEntity e = new AbstractEntity("Wall");

		ImageComponent imageData = new ImageComponent("greensquare.png", 0, w, h);
		e.addComponent(imageData);

		PositionComponent pos = PositionComponent.ByBottomLeft(x, y, w, h);
		e.addComponent(pos);
		CollisionComponent cc = new CollisionComponent(true, true);
		e.addComponent(cc);

		return e;
	}


	public AbstractEntity createHarmfulArea(int x, int y, float w, float h) {
		AbstractEntity e = new AbstractEntity("HarmfulArea");

		ImageComponent imageData = new ImageComponent("lightgreensquare.png", 0, w, h);
		e.addComponent(imageData);

		PositionComponent pos = PositionComponent.ByBottomLeft(x, y, w, h);
		e.addComponent(pos);
		CollisionComponent cc = new CollisionComponent(true, false);
		e.addComponent(cc);
		HarmOnContactComponent hoc = new HarmOnContactComponent();
		e.addComponent(hoc);
		return e;
	}

	
	public AbstractEntity createImage(String filename, int x, int y, float w, float h, int zOrder) {
		AbstractEntity e = new AbstractEntity("Image_" + filename);

		ImageComponent imageData = new ImageComponent(filename, zOrder, w, h);
		e.addComponent(imageData);
		PositionComponent pos = PositionComponent.ByBottomLeft(x, y, w, h);
		e.addComponent(pos);

		return e;
	}


	public AbstractEntity createAlien(int x, int y) {
		AbstractEntity e = new AbstractEntity("Mob");

		ImageComponent imageData = new ImageComponent("redblip.png", 1, Settings.PLAYER_SIZE, Settings.PLAYER_SIZE);
		e.addComponent(imageData);
		PositionComponent pos = PositionComponent.ByBottomLeft(x, y, Settings.PLAYER_SIZE, Settings.PLAYER_SIZE);
		e.addComponent(pos);
		CollisionComponent cc = new CollisionComponent(true, true);
		e.addComponent(cc);
		MovementComponent mc = new MovementComponent();
		e.addComponent(mc);
		AlienComponent mob = new AlienComponent(45);
		e.addComponent(mob);
		return e;
	}


}
