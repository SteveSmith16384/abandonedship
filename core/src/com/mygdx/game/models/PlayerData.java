package com.mygdx.game.models;

import com.badlogic.gdx.controllers.Controller;
import com.mygdx.game.Settings;
import com.scs.basicecs.AbstractEntity;

public class PlayerData {

	private static int nextImageId = 1;

	public Controller controller; // If null, player is keyboard
	private boolean in_game = false;
	public boolean quit = false; // If they've removeed their controller; prevent them re-attaching to start again
	public AbstractEntity avatar;
	public int imageId;

	public PlayerData(Controller _controller) {
		this.controller = _controller;
	}


	public void setInGame(boolean b) {
		if (b) {
			if (Settings.RELEASE_MODE == false) {
				if (in_game) {
					throw new RuntimeException("Player already in game!");
				}
			}
			this.in_game = true;
			imageId = nextImageId++;
		} else {
			this.in_game = false;
		}
	}


	public boolean isInGame() {
		return this.in_game;
	}

}
