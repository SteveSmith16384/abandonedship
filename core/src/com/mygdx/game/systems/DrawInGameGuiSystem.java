package com.mygdx.game.systems;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.MyGdxGame;

public class DrawInGameGuiSystem {

	private MyGdxGame game;
	private SpriteBatch batch;
	private Sprite[] players = new Sprite[3];

	public DrawInGameGuiSystem(MyGdxGame _game, SpriteBatch _batch) {
		game = _game;
		batch = _batch;
	}


	public void process() {
	}
}
