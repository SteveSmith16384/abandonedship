package com.mygdx.game.systems;
/*
import com.badlogic.gdx.math.GridPoint2;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Settings;
import com.mygdx.game.components.AlienComponent;
import com.mygdx.game.components.MovementComponent;
import com.scs.astar.Waypoints;
import com.scs.awt.Point;
import com.scs.awt.PointF;
import com.scs.basicecs.AbstractEntity;
import com.scs.basicecs.AbstractSystem;
import com.scs.basicecs.BasicECS;
import com.scs.lang.NumberFunctions;

public class AlienAISystem extends AbstractSystem {

	private MyGdxGame game;
	private GridPoint2 destPixels;
	private Waypoints route;
	
	public AlienAISystem(MyGdxGame _game, BasicECS ecs) {
		super(ecs);

		game = _game;
	}


	@Override
	public Class getEntityClass() {
		return AlienComponent.class;
	}


	@Override
	public void processEntity(AbstractEntity entity) {
		MovementComponent move = (MovementComponent)entity.getComponent(MovementComponent.class);
		//move.offX = mob.dirX * mob.speed;
	}

	
	private void getRandomDest() {
		int x = NumberFunctions.rnd((int)(Settings.SQ_SIZE*1), (int)(game.mapEntity.mapdata.getMapWidth() * Settings.SQ_SIZE));
		int y = NumberFunctions.rnd((int)(Settings.SQ_SIZE*1), (int)(game.mapEntity.mapdata.getMapHeight() * Settings.SQ_SIZE));
		this.setDest(x, y);

	}

	protected void moveToDestination(float tpfSecs) {
		if (destPixels != null) {
			float dist = destPixels.getDistance(this.getWorldCentreX(), this.getWorldCentreY());
			if (dist < 1) { // Stop them when reach destination
				endMovement();
				return;
			}			

			if (route == null) {
				PointF offset = this.destPixels.subtract(this.getWorldCentreX(), this.getWorldCentreY());
				offset.normalizeLocal();
				offset.multLocal(tpfSecs * stats.speed);
				move(offset);
			} else {
				Point nextMapPoint = route.getNextPoint();
				PointF pixelDest = new PointF(nextMapPoint.x * Settings.SQ_SIZE + (Settings.SQ_SIZE/2), nextMapPoint.y * Settings.SQ_SIZE + (Settings.SQ_SIZE/2));
				float d = pixelDest.getDistance(this.getWorldCentreX(), this.getWorldCentreY());
				if (d < 5) { // Must be close otherwise we'll start walking to the next square too soon and try to cut corners which means
					route.removeCurrentPoint();
					if (route.isEmpty()) {
						endMovement();
					}
				} else {
					PointF ourPos = new PointF(this.getWorldCentreX(), this.getWorldCentreY());
					PointF offset = pixelDest.subtract(ourPos);
					offset.normalizeLocal();
					offset.multLocal(tpfSecs * stats.speed);
					move(offset);
				}
			}
		} else { // No destination
			// todo move away from each other if on top
		}
	}

	
	private void endMovement() {
		this.route = null;
	}

}
*/