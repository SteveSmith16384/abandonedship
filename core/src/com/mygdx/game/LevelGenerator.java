package com.mygdx.game;

import com.mygdx.game.mapgen.AbstractDungeon.SqType;
import com.mygdx.game.mapgen.DungeonGen1;
import com.scs.basicecs.AbstractEntity;
import com.scs.basicecs.BasicECS;

public class LevelGenerator {

	private EntityFactory entityFactory;
	private BasicECS ecs;

	public LevelGenerator(EntityFactory _entityFactory, BasicECS _ecs) {
		entityFactory = _entityFactory;
		ecs = _ecs;
	}


	public void createLevel1() {
		DungeonGen1 gen = new DungeonGen1(40, 5, 10, 1);
		SqType[][] map = gen.map;

		int w = map[0].length;
		int h = map.length;
		for(int my=0 ; my< h ; my++) {
			for(int mx=0 ; mx<w ; mx++) {
				int px = (int)(mx * Settings.SQ_SIZE);
				int py = (int)(my * Settings.SQ_SIZE);

				if (map[mx][my] == SqType.DOOR_EW || map[mx][my] == SqType.DOOR_NS) {
				} else if (map[mx][my] == SqType.WALL) {
					AbstractEntity e = entityFactory.createWall(px, py, Settings.SQ_SIZE, Settings.SQ_SIZE);
					ecs.addEntity(e);
				} else if (map[mx][my] == SqType.FLOOR) {
				} else if (map[mx][my] == SqType.COMPUTER) {
					//entityFactory.createPlayersAvatar(player, null, mx, my);
				} else {
				}
			}
		}
	}


}
