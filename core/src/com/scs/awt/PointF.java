package com.scs.awt;

public class PointF {

	public float x, y;

	public PointF() {
		this(0, 0);
	}


	public PointF(float x, float y) {
		this.x = x;
		this.y = y;
	}


	public float length() {
		return (float)Math.sqrt(x * x + y * y);
	}


	public void normalizeLocal() {
		float len = length();
		if (len != 0) {
			x /= len;
			y /= len;
		}
	}


	public PointF subtract(PointF o) {
		PointF newone = new PointF(this.x, this.y);
		
		newone.x -= o.x;
		newone.y -= o.y;
		
		return newone;
	}

	
	public PointF multiply(float o) {
		PointF newone = new PointF(this.x, this.y);
		
		newone.x *= o;
		newone.y *= o;
		
		return newone;
	}

	
}
